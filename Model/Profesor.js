const con = require('./db');
var Profesores = function(params){
    this.idColaboradores = params.idColaboradores;
    this.Contrato = params.Contrato;
    this.Usuarios_idUsuarios = params.Usuarios_idUsuarios;
    this.Secciones_idSecciones = params.Secciones_idSecciones;
    //Foros
    this.Colaboradores_idColaboradores = params.Colaboradores_idColaboradores;
    this.Materias_idMaterias=params.Materias_idMaterias;
    this.Descripcion=params.Descripcion;
    //criterios
    this.Criterio = params.Criterio;
    this.Valor=params.Valor;
    //Tareas
    this.Nombre=params.Nombre;
    this.Bibliografia = params.Bibliografia;
    this.RutaArchivo = params.RutaArchivo;
    this.FechaPublicacion=params.FechaPublicacion;
    this.FechaEntrega=params.FechaEntrega;
    this.HoraLimite=params.HoraLimite;
    //Asistencias participaciones
    this.AsistenciaParticipacion_idAsistenciaParticipacion = params.AsistenciaParticipacion_idAsistenciaParticipacion;
    this.Alumnos_Matricula = params.Alumnos_Matricula;
    this.Fecha = params.Fecha;
    this.Asistencia = params.Asistencia;
    this.Participacion = params.Participacion; 
}

Profesores.getMaterias = function(id,result){

    con.query(
        'Select Materias.Nombre,Secciones.Seccion,Grados.Grado,Grupos.Grupo from Materias INNER JOIN Grupos ON Materias.Grupos_idGrupos=Grupos.idGrupos INNER JOIN Grados ON Grupos.Grados_idGrados=Grados.idGrados INNER JOIN Secciones ON Grados.Secciones_idSecciones=Secciones.idSecciones where Colaboradores_idColaboradores = ?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getHorarios = function(id,result){

    con.query(
        'SELECT * FROM Horarios INNER JOIN Materias ON Horarios.Materias_idMaterias=Materias.idMaterias WHERE Materias.Colaboradores_idColaboradores = ?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getForos = function(id,result){

    con.query(
        'SELECT Foros.Descripcion,Materias.Nombre,Secciones.Seccion,Grados.Grado,Grupos.Grupo FROM `Foros` INNER JOIN Materias ON Foros.Materias_idMaterias=Materias.idMaterias INNER JOIN Grupos ON Materias.Grupos_idGrupos=Grupos.idGrupos INNER JOIN Grados ON Grupos.Grados_idGrados =Grados.idGrados INNER JOIN Secciones ON Grados.Secciones_idSecciones=Secciones.idSecciones WHERE Foros.Colaboradores_idColaboradores=? AND Foros.Activo=1',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getForosInactivo = function(id,result){

    con.query(
        'SELECT Foros.Descripcion,Materias.Nombre,Secciones.Seccion,Grados.Grado,Grupos.Grupo FROM `Foros` INNER JOIN Materias ON Foros.Materias_idMaterias=Materias.idMaterias INNER JOIN Grupos ON Materias.Grupos_idGrupos=Grupos.idGrupos INNER JOIN Grados ON Grupos.Grados_idGrados =Grados.idGrados INNER JOIN Secciones ON Grados.Secciones_idSecciones=Secciones.idSecciones WHERE Foros.Colaboradores_idColaboradores=? AND Foros.Activo=0',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.postForos = function(id,result){
    con.query(
        'INSERT INTO `Foros`(`Colaboradores_idColaboradores`, `Materias_idMaterias`, `Descripcion`, `Activo`) VALUES ('+id.Colaboradores_idColaboradores+','+id.Materias_idMaterias+',"'+id.Descripcion+'",1)',function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.postCriterios = function(id,result){
    con.query(
        'INSERT INTO `CriteriosEvaluacion`(`Criterio`, `Valor`, `Materias_idMaterias`) VALUES ("'+id.Criterio+'",'+id.Valor+','+id.Materias_idMaterias+')',function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getCriterios = function(id,result){

    con.query(
        'SELECT * FROM `CriteriosEvaluacion` WHERE Materias_idMaterias= ?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getTareas = function(id,result){

    con.query(
        'SELECT * FROM `Tareas` WHERE Materias_idMaterias= ?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.postTareas = function(id,result){
    con.query(
        'INSERT INTO `Tareas`(`Nombre`, `Materias_idMaterias`, `Descripcion`, `Bibliografia`, `RutaArchivo`, `FechaPublicacion`, `FechaEntrega`,`HoraLimite`) VALUES ("'+id.Nombre+'",'+id.Materias_idMaterias+',"'+id.Descripcion+'","'+id.Bibliografia+'","'+id.RutaArchivo+'","'+id.FechaPublicacion+'","'+id.FechaEntrega+'","'+id.HoraLimite+'")',function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getDetalleTarea = function(id,result){

    con.query(
        'SELECT * FROM `Tareas` WHERE idTareas= ?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getAlumnosAbonosPagados = function(id,result){

    con.query(
        'SELECT * FROM Abonos INNER JOIN Alumnos on Abonos.Alumnos_Matricula=Alumnos.Matricula INNER JOIN Usuarios on Alumnos.Usuarios_idUsuarios=Usuarios.idUsuarios INNER JOIN Pagos ON Abonos.Pagos_idPagos=Pagos.idPagos INNER JOIN Grupos ON Alumnos.Grupos_idGrupos=Grupos.idGrupos INNER JOIN Materias ON Materias.Grupos_idGrupos=Grupos.idGrupos WHERE Materias.idMaterias= ?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getAsistenciaParticipacion = function(id,result){

    con.query(
        'SELECT * FROM AsistenciaParticipacion_has_Alumnos INNER JOIN AsistenciaParticipacion ON AsistenciaParticipacion_has_Alumnos.AsistenciaParticipacion_idAsistenciaParticipacion=AsistenciaParticipacion.idAsistenciaParticipacion INNER JOIN Alumnos ON AsistenciaParticipacion_has_Alumnos.Alumnos_Matricula=Alumnos.Matricula INNER JOIN Usuarios ON Alumnos.Usuarios_idUsuarios=Usuarios.idUsuarios WHERE AsistenciaParticipacion.Materias_idMaterias= ?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.postAsistenciaParticipacion = function(id,result){
    con.query(
        'INSERT INTO `AsistenciaParticipacion_has_Alumnos`(`AsistenciaParticipacion_idAsistenciaParticipacion`, `Alumnos_Matricula`, `Fecha`, `Asistencia`, `Participacion`) VALUES ('+id.AsistenciaParticipacion_idAsistenciaParticipacion+','+id.Alumnos_Matricula+',"'+id.Fecha+'","'+id.Asistencia+'","'+id.Participacion+'")',function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getCalifTareas = function(id,result){

    con.query(
        'SELECT * FROM `TareasAlumnos` INNER JOIN Tareas ON TareasAlumnos.Tareas_idTareas=Tareas.idTareas INNER JOIN Alumnos ON TareasAlumnos.Alumnos_Matricula=Alumnos.Matricula INNER JOIN Usuarios ON Alumnos.Usuarios_idUsuarios=Usuarios.idUsuarios WHERE Tareas.Materias_idMaterias= ?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Profesores.getHorarios = function(id,result){

    con.query(
        'SELECT * FROM `Horarios` LEFT JOIN Materias ON Horarios.Materias_idMaterias=Materias.idMaterias INNER JOIN Colaboradores ON Colaboradores.idColaboradores=Horarios.Colaboradores_idColaboradores INNER JOIN Usuarios ON Colaboradores.Usuarios_idUsuarios1=Usuarios.idUsuarios WHERE Colaboradores.idColaboradores=?',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
module.exports = Profesores;