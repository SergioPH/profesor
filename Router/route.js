module.exports = function(app){
    const Profesor = require('../Controller/Profesor')
    app.route('/InicioProfesor/:id').get(Profesor.getMaterias);
    app.route('/InicioProfesorF/:id').get(Profesor.getForos);
    app.route('/InicioProfesorFI/:id').get(Profesor.getForosInactivo);
    app.route('/InicioProfesorH/:id').get(Profesor.getHorarios);
    app.route('/GenerarForo').post(Profesor.postForos);
    app.route('/Criterios').post(Profesor.postCriterios);
    app.route('/ListaCriterios/:id').get(Profesor.getCriterios);
    app.route('/ListaTareas/:id').get(Profesor.getTareas);
    app.route('/AltaTareas').post(Profesor.postTareas);
    app.route('/DetalleTarea/:id').get(Profesor.getDetalleTarea);
    app.route('/DerechoAExamen/:id').get(Profesor.getAlumnosAbonosPagados);
    app.route('/AsistenciaParticipacion/:id').get(Profesor.getAsistenciaParticipacion);
    app.route('/AsistenciaParticipaciones').post(Profesor.postAsistenciaParticipacion);
    app.route('/ListaCalificacionTareas/:id').get(Profesor.getCalifTareas);
    app.route('/HorarioProfesor/:id').get(Profesor.getHorarios);
}