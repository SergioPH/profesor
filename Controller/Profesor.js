const ProfeModel = require('../Model/Profesor');
exports.getMaterias = (req,res) =>{
    ProfeModel.getMaterias(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getHorarios = (req,res) =>{
    ProfeModel.getHorarios(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getForos = (req,res) =>{
    ProfeModel.getForos(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getForosInactivo = (req,res) =>{
    ProfeModel.getForosInactivo(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.postForos = (req,res) =>{
    var modelG = new ProfeModel(req.body)
    if(modelG == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        ProfeModel.postForos(modelG,function(error,modelG){
            if(error)
                res.send(error)
            res.send(modelG)
        })   
    }
}
exports.postCriterios = (req,res) =>{
    var modelG = new ProfeModel(req.body)
    if(modelG == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        ProfeModel.postCriterios(modelG,function(error,modelG){
            if(error)
                res.send(error)
            res.send(modelG)
        })   
    }
}
exports.getCriterios = (req,res) =>{
    ProfeModel.getCriterios(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getTareas = (req,res) =>{
    ProfeModel.getTareas(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.postTareas = (req,res) =>{
    var modelG = new ProfeModel(req.body)
    if(modelG == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        ProfeModel.postTareas(modelG,function(error,modelG){
            if(error)
                res.send(error)
            res.send(modelG)
        })   
    }
}
exports.getDetalleTarea = (req,res) =>{
    ProfeModel.getDetalleTarea(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getAlumnosAbonosPagados = (req,res) =>{
    ProfeModel.getAlumnosAbonosPagados(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getAsistenciaParticipacion = (req,res) =>{
    ProfeModel.getAsistenciaParticipacion(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.postAsistenciaParticipacion = (req,res) =>{
    var modelG = new ProfeModel(req.body)
    if(modelG == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        ProfeModel.postAsistenciaParticipacion(modelG,function(error,modelG){
            if(error)
                res.send(error)
            res.send(modelG)
        })   
    }
}
exports.getCalifTareas = (req,res) =>{
    ProfeModel.getCalifTareas(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getHorarios = (req,res) =>{
    ProfeModel.getHorarios(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}